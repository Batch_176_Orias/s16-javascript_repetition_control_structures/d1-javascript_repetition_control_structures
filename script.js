console.log(`Hello World`);

//while loop

function whiles(){
	/*let c = 3;

	while(c != 0){
		console.log(`While ${c}`);

		c--;
	}*/

	// mini act

	let x = 0;

	while (x <= 5){
		console.log(x);
		x++;
	}
}

// whiles();

function dowhiles(){
	let n = Number(prompt(`Enter #`));

	do{
		console.log(n);
		n++;
	}
	while(n < 10)
}

// dowhiles();

function fors(){

	let str = `Marvin`;
	for(let x = 0; x < str.length; x++){
		// console.log(`For looooooooooop ${str[x]}`);
		if(str[x].toLowerCase() == `a` ||
			str[x].toLowerCase() == `e` ||
			str[x].toLowerCase() == `i` ||
			str[x].toLowerCase() == `o` ||
			str[x].toLowerCase() == `u`){
			console.log(`Vowel`);
		}
		else{
			console.log(str[x]);
		}
	}
}

// fors();

//continue and break(for)

function fors1(){
	for (let x = 0; x<= 20; x++){
		if (x % 2 === 0){
			console.warn(`Even: ${x}`);
			continue;
		}

		console.error(`Continue and break: ${x}`);

		if (x > 10){
			break;
		}
	}
}
// fors1();

function ex1(){
	let name = `kamui`;

	for (let x = 0; x <= name.length-1; x++){
		console.warn(name[x]);

		if (name[x] === `a`){
			console.log(`Continue`);
			continue;
		}

		if (name[x] === `u`){
			break;
		}
	}
}

ex1();